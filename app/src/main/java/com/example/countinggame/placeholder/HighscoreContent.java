package com.example.countinggame.placeholder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class HighscoreContent {

    /**
     * An array of sample high score items.
     */
    public static final List<PersonalHighscore> ITEMS = new ArrayList<PersonalHighscore>();

    /**
     * A map of sample high score items, by ID.
     */
    public static final Map<String, PersonalHighscore> HIGHSCORE_MAP = new HashMap<String, PersonalHighscore>();

    private static final int COUNT = 10;

    static {
        // Adding some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createNewHighscore(i));
        }
    }

    //I know I am making spaghetti and I am not proud of it.
    private static void addItem(PersonalHighscore item) {
        ITEMS.add(item);

        //Adds to hashmap, dont need it.
        //HIGHSCORE_MAP.put(item.id, item);
    }

    public static void addNewHighscore(String name, int highScore) {
        int newPosition = getNewPosition(highScore);
        String position = (String.valueOf(newPosition + 1));
        PersonalHighscore newEntry = new PersonalHighscore(position, name, highScore);

        shiftHighscores(newPosition);
        ITEMS.set(newPosition, newEntry);
        updatePos();

    }

    private static void shiftHighscores(int startPos) {
        for (int i = (ITEMS.size() - 1); (i > startPos) && (i > 0); i--) {
            ITEMS.set(i, ITEMS.get(i - 1));
        }
    }

    private static void updatePos() {
        for (int i = 0; i < (ITEMS.size()); i++) {
            ITEMS.get(i).setId(Integer.toString(i + 1));
        }

    }

    private static int getNewPosition(int newScore) {
        boolean found = false;
        int returnValue = 0;

        for (int i = ITEMS.size() - 1; i > 0 && !found; i--) {
            if (newScore > ITEMS.get(i - 1).highscore) {
                returnValue = (i - 1);
            }
        }
        return returnValue;

    }

    public static int getLastHighScore() {
        return ITEMS.get(COUNT - 1).highscore;
    }

    private static PersonalHighscore createNewHighscore(int position) {
        return new PersonalHighscore(String.valueOf(position), "Item " + position, 10);
    }

    /**
     * Item that contains a personal highscore with name, id, and highscore.
     *
     */
    public static class PersonalHighscore {
        public String id;
        public String name;
        public int highscore;

        public PersonalHighscore(String id, String name, int highscore) {
            this.id = id;
            this.name = name;
            this.highscore = highscore;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}