package com.example.countinggame;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.countinggame.placeholder.HighscoreContent.PersonalHighscore;
import com.example.countinggame.databinding.FragmentHighscoreBinding;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link PersonalHighscore}.
 *
 */
public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private final List<PersonalHighscore> mValues;

    public MyItemRecyclerViewAdapter(List<PersonalHighscore> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return new ViewHolder(FragmentHighscoreBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText(mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).name);
        holder.mHighScore.setText(Integer.toString(mValues.get(position).highscore));
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView mIdView;
        public final TextView mContentView;
        public final TextView mHighScore;
        public PersonalHighscore mItem;

        public ViewHolder(FragmentHighscoreBinding binding) {
            super(binding.getRoot());
            mIdView = binding.itemNumber;
            mContentView = binding.content;
            mHighScore = binding.highscore;
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}