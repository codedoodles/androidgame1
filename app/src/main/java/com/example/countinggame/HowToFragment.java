package com.example.countinggame;

import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class HowToFragment extends Fragment {

    TextView androidVers;

    public HowToFragment() {
        // Required empty public constructor
    }

    public static HowToFragment newInstance() {
        HowToFragment fragment = new HowToFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_how_to, container, false);
        String androidV = Build.VERSION.RELEASE;
        int sdkV = Build.VERSION.SDK_INT;

        androidVers = (TextView) v.findViewById(R.id.androidVersion);
        androidVers.setText("Android version: " + androidV + " (SDK: "+ sdkV + ")");
        return v;
    }
}
