package com.example.countinggame;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView callFragment;
    TextView highScoreButton;
    TextView startGameButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        callFragment = findViewById(R.id.howTo);
        callFragment.setOnClickListener(this::howTo);
        highScoreButton = findViewById(R.id.highScoreMenu);
        highScoreButton.setOnClickListener(this::viewHighscore);
        startGameButton = findViewById(R.id.startGame);
        startGameButton.setOnClickListener(this::startGame);
    }

    public void howTo(View view) {
        getSupportFragmentManager().beginTransaction().add(R.id.container, new HowToFragment()).addToBackStack("my_fragment").commit();
    }

    @Override
    public void onClick(View v) {
        getSupportFragmentManager().beginTransaction().add(R.id.container, new HowToFragment()).addToBackStack("my_fragment").commit();
    }

    public void viewHighscore(View v) {
        getSupportFragmentManager().beginTransaction().add(R.id.container, new highscoreScreenFragment()).addToBackStack("high_score").commit();
    }

    public void startGame(View v) {
        Intent intent = new Intent(this, GameActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }
}