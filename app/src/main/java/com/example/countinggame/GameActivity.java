package com.example.countinggame;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.countinggame.objects.mathEquation;
import com.example.countinggame.placeholder.HighscoreContent;

public class GameActivity extends AppCompatActivity {

    int mathType;
    int num1, num2;
    int answer;
    String userInput2;
    String myString;
    Button submitButton;
    Button nextButton;
    mathEquation equate;

    TextView textQuestion;
    TextView textAnswer;
    TextView correctAnswers;
    TextView totalQuestions;
    TextView highScoreView;

    final int MAX_QUESTIONS = 4;
    final int RIGHT_ANSWER = 10;
    final int WRONG_ANSWER = -5;
    int highScore = 0;
    int correctTotal = 0;
    int wrongTotal = 0;
    int questionsTotal = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        equate = new mathEquation();

        //Populating all text views with contents of variables that can be updated during gameplay.
        correctAnswers = (TextView) findViewById(R.id.correctTotalNr);
        correctAnswers.setText(String.valueOf(correctTotal));

        textQuestion = (TextView) findViewById(R.id.textViewQuestion);
        textQuestion.setText(myString);

        totalQuestions = (TextView) findViewById(R.id.questionsTotal);
        totalQuestions.setText(String.valueOf(questionsTotal));

        highScoreView = (TextView) findViewById(R.id.highscoreTextView);
        highScoreView.setText(String.valueOf(highScore));

        textAnswer = (TextView) findViewById(R.id.textAnswer);
        this.createNewQuestion();

       nextButton = (Button) findViewById(R.id.nextButton);

        // Grab user input
        EditText simpleEditText = (EditText) findViewById(R.id.editTextNumberSigned);

        submitButton = findViewById(R.id.submitButton);

        //Not proud of this bit, needs refactoring. But if it works, it works. Time for refactoring later.
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userInput2 = simpleEditText.getText().toString();
                int userInput3 = Integer.parseInt(userInput2);

                //If answer is right
                if(userInput3 == answer){
                    textAnswer.setText("You answered correctly!");
                    correctTotal++;
                    highScore += RIGHT_ANSWER;
                    correctAnswers.setText(String.valueOf(correctTotal));
                    hideSubmit();

                } else {   // If answer is wrong.
                    textAnswer.setText("Wrong answer.");
                    wrongTotal++;
                    hideSubmit();
                    if(highScore < 5){
                        highScore = 0;
                    } else {
                        highScore += WRONG_ANSWER;
                    }
                }
                highScoreView.setText(String.valueOf(highScore));
            }
        });

        //If the next button is clicked, hide the next button, and show a new question.
        nextButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                int last = HighscoreContent.getLastHighScore();
                if(questionsTotal < MAX_QUESTIONS) {
                    showSubmit();
                    simpleEditText.setText(null);
                    createNewQuestion();
                    questionsTotal++;
                    totalQuestions.setText(String.valueOf(questionsTotal));

                } else {
                    if(highScore > last){
                        nextButton.setVisibility(View.GONE);
                        submitButton.setVisibility(View.GONE);

                        //Initialise fragment for entering high score.
                        EnterHighscoreFragment fragment = new EnterHighscoreFragment();
                        Bundle highScoreBundle = new Bundle();
                        highScoreBundle.putInt("score", highScore);
                        fragment.setArguments(highScoreBundle);

                        //Use fragment transaction to transfer info
                        final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                        final String FRAGMENT_TAG = "highscore";
                        ft.replace(R.id.gameContainer, fragment, FRAGMENT_TAG);
                        ft.commit();
                    } else {
                        System.out.println("The highscore was not higher. Current: " + highScore + "  List lowest: " + last);
                    }
                    textQuestion.setText("Game Over. Please go back and try again. ");
                }
            }
        });
    }

    public void hideSubmit(){
        submitButton.setVisibility(View.INVISIBLE);
        nextButton.setVisibility(View.VISIBLE);
    }

    public void showSubmit(){
        submitButton.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.INVISIBLE);
    }

    public void createNewQuestion(){
            mathType = getRand(3);
            num1 = getRand(11);
            num2 = getRand(11);

            myString = equate.createEquation(mathType, num1, num2);
            answer = equate.answer;
        totalQuestions.setText(String.valueOf(questionsTotal));
        textQuestion.setText(myString);
    }

    public int getRand(int maxVal) {
        int randomNumber = (int) (Math.random() * (maxVal));
        return randomNumber;
    }
}