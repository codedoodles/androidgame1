package com.example.countinggame;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.example.countinggame.placeholder.HighscoreContent;

public class EnterHighscoreFragment extends Fragment {

    private String name;
    private int highscore;
    Button submitHighscore;
    EditText enterName;

    public EnterHighscoreFragment() {
        // Required empty public constructor
    }

    public static EnterHighscoreFragment newInstance() {
        EnterHighscoreFragment fragment = new EnterHighscoreFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        if(arguments != null){
            highscore = arguments.getInt("score");
        } else {
            System.out.println("The bundle was null.");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_enter_highscore, container, false);

        submitHighscore = (Button) view.findViewById(R.id.submitHighscore);
        enterName = (EditText) view.findViewById(R.id.editHighscoreName);
        enterName.setText("");
        submitHighscore.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                name = enterName.getText().toString();
                HighscoreContent.addNewHighscore(name, highscore);
                getActivity().onBackPressed();

            }
        });

        return view;
    }
}