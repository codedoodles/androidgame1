package com.example.countinggame.objects;

import java.util.Locale;

public class mathEquation {
    int mathType;
    public int answer;
    String myString;
    String operatorType;

    public String createEquation(int mt, int num1, int num2) {
        mathType = mt;
        switch (mathType) {
            case 0:
                // Addition
                operatorType = " + ";
                answer = num1 + num2;
                myString = getString(num1, num2);
                break;
            case 1:
                // Subtraction
                operatorType = " - ";
                answer = num1 - num2;
                myString = getString(num1, num2);
                break;
            case 2:
                // Multiplication
                operatorType = " * ";
                answer = num1 * num2;
                myString = getString(num1, num2);
                break;

            default:
                System.out.println("Error.");
                break;
                // Error?
        }
        return myString;
    }

    public String getString(int val1, int val2){
        return String.format(Locale.getDefault(), "%s %d %s %d %s",
                "What is ",
                val1,
                operatorType,
                val2,
                "? \n");
    }
}
