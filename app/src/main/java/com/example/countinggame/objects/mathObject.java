package com.example.countinggame.objects;


public class mathObject {
    int value;

    mathObject(String[] imageArray, int max, int min) {
        setMathObjectValue(max, min);
    }

    public mathObject getMathObject() {
        return this;
    }

    public int getMathValue() {
        return value;
    }

    public void setMathObjectValue(int max, int min) {
        value = (int) (Math.random() * (max-min+1)+min);
    }

    public void setMathObjectImage(){

    }
}
