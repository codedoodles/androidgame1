package com.example.countinggame.objects;

//Different types of mathematical operations for game modes.

public enum Operation {
    ADDITION,
    SUBTRACTION,
    MULTIPLICATION,
    DIVISION
}

